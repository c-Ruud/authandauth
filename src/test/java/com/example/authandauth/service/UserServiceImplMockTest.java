package com.example.authandauth.service;

import com.example.authandauth.domain.Resource;
import com.example.authandauth.domain.Right;
import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.entity.ResourcesRightsEntity;
import com.example.authandauth.entity.UserEntity;
import com.example.authandauth.exceptions.Activity;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.exceptions.UseExceptionType;
import com.example.authandauth.repository.SaltRepository;
import com.example.authandauth.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("integrationtest")
class UserServiceImplMockTest {

    @Autowired
    UserServiceImpl userService;
    @MockBean
    UserRepository userRepository;
    @MockBean
    SaltRepository saltRepository;


    UserEntity anna;
    UserEntity berit;
    UserEntity kalle;


    @BeforeEach
    void setUp() {
        anna = new UserEntity("1", "anna", "VDmBRebCtWxQzW2hIXscH7pgJ8rkEDaiOqkM0qHQuOrle6wf0z9Ro3Rkf3Vr+z3Xl6jrVRNjkxeERB6Uifg5Yw==", "123");
        berit = new UserEntity("2", "berit", "Qn6mgYbL09jUhIv/UuNHbzxJtNW7TEhohPrpHd4ttsCyt1CdUpzHTQbZxVRSwoJBhcvWwJ3feNUWVSERgGlk9A==", "123");
        kalle = new UserEntity("3", "kalle", "ViSmZYcwsLmR8rELuAZE3G/SiEDZ0gvYSdlQCdeaLrYdD0NSyd6sH7U4OkKIjR0mwC+/xsCjBvoW9+ikNS4toQ==", "123");
    }

    @Test
    void saveUser() throws UseException {
        //given
        when(userRepository.save(eq(anna))).thenReturn(anna);

        //then
        UserEntity annaUser = userService.saveUser(new SetUserInfo("anna", "losen"));

        assertNotNull(annaUser.getToken());
        assertEquals("anna", annaUser.getUsername());
    }

    @Test
    void test_verify_login_and_return_token_success() throws UseException {
        //Given
        when(userRepository.getById(eq(anna.getToken()))).thenReturn(anna);
        when(userRepository.findAll()).thenReturn(List.of(anna));

        //When
        System.out.println(anna.getPassword());
        System.out.println(anna.getUsername());
        String loginAnna = userService.verifyLogin(anna.getUsername(), "losen");

        //Then
        assertEquals(anna.getToken(), loginAnna);
    }

    @Test
    void testa_to_get_salt_from_user_for_further_testing() {
        //given
        when(userRepository.getById(anna.getToken())).thenReturn((anna));
        when(userRepository.findAll()).thenReturn(List.of(anna));

        //when
        String annaSalt = userService.getSalt(anna.getToken());
        System.out.println(annaSalt);
        String annaPassword = userService.hashPassword("losen", annaSalt);

        //then
        assertEquals("123", annaSalt);
        assertEquals("VDmBRebCtWxQzW2hIXscH7pgJ8rkEDaiOqkM0qHQuOrle6wf0z9Ro3Rkf3Vr+z3Xl6jrVRNjkxeERB6Uifg5Yw==", annaPassword);

    }

    @Test
    void test_verify_login_fail() {
        //Given
        when(userRepository.getById(berit.getToken())).thenReturn((berit));
        when(userRepository.findAll()).thenReturn(List.of(berit));


        //When
        UseException userException = assertThrows(UseException.class, () -> userService.verifyLogin("berit", "12345"));

        //Then
        assertNotNull(userException);
        assertThat(userException.getActivity(), is(Activity.VERIFY_USER));
        assertThat(userException.getUserExceptionType(), is(UseExceptionType.USER_PASSWORD_INCORRECT));
    }

    @Test
    void verifyToken() {
        //Givet
        when(userRepository.save(eq(berit))).thenReturn(berit);
        when(userRepository.findAll()).thenReturn(List.of(berit));
        //När
        boolean verifyBerit = userService.verifyToken(berit.getToken());
        //Då
        assertTrue(verifyBerit);
    }

    @Test
    void getToken() {
        //Given
        when(userRepository.save(eq(kalle))).thenReturn(kalle);

        //Then
        assertEquals("3", kalle.getToken());
    }

    @Test
    void getUsers() {
        //given
        when(userRepository.save(eq(anna))).thenReturn(anna);
        when(userRepository.save(eq(berit))).thenReturn(berit);
        when(userRepository.save(eq(kalle))).thenReturn(kalle);
        when(userRepository.findAll()).thenReturn(List.of(anna, berit, kalle));

        //When
        List<UserEntity> usersList = userService.getUsers().collect(Collectors.toList());

        //Then
        assertEquals(3, usersList.size());
        assertEquals("kalle", usersList.get(2).getUsername());
        assertEquals("1", usersList.get(0).getToken());
    }

    @Test
    void addResourceAndRight() {
        //given
        when(userRepository.save(eq(anna))).thenReturn(anna);

        //when
        anna.addResourceAndRight(new ResourcesRightsEntity(UUID.randomUUID().toString(), Resource.ACCOUNT, Right.READ, anna));

        //then
        assertEquals("anna", anna.getUsername());
        assertEquals(Resource.ACCOUNT, anna.getResourcesRights().get(0).getResource());
        assertEquals(Right.READ, anna.getResourcesRights().get(0).getRight());
    }

    @Test
    void getRightsByResourceFromToken() throws UseException {
        //given
        when(userRepository.save(eq(anna))).thenReturn(anna);
        when(userRepository.getById(eq(anna.getToken()))).thenReturn(anna);
        anna.addResourceAndRight(new ResourcesRightsEntity(UUID.randomUUID().toString(), Resource.ACCOUNT, Right.READ, anna));

        //when
        List<Right> rights = userService.getRightsByResourceFromToken(anna.getToken(), Resource.ACCOUNT);

        assertEquals(1, rights.size());
    }
}
