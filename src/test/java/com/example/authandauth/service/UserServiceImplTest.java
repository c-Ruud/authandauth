package com.example.authandauth.service;

import com.example.authandauth.domain.Resource;
import com.example.authandauth.domain.Right;
import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.entity.ResourcesRightsEntity;
import com.example.authandauth.entity.UserEntity;
import com.example.authandauth.exceptions.Activity;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.exceptions.UseExceptionType;
import com.example.authandauth.repository.ResourcesRightsRepository;
import com.example.authandauth.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("integrationtest")
class UserServiceImplTest {
    public static final String ANSI_RED = "\u001B[31m";

    public static final String ANSI_RESET = "\033[0m";


    @Autowired
    UserRepository userRepository;
    @Autowired
    UserServiceImpl userService;
    @Autowired
    ResourcesRightsRepository resourcesRightsRepository;


    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }


    @Test
    void test_save_user_success() throws UseException {
        //Given
        UserEntity user = userService.saveUser(new SetUserInfo("kalle", "password"));

        //Then
        assertNotNull(user.getToken());
        assertEquals("kalle", user.getUsername());
        assertNotNull(user.getPassword());
    }

    @Test
    void test_save_berit_success() throws UseException {
        //Given
        UserEntity user = userService.saveUser(new SetUserInfo("berit", "123456"));

        //Then
        assertNotNull(user.getToken());
        assertEquals("berit", user.getUsername());
        assertNotNull(user.getPassword());
    }

    @Test
    void test_verify_that_username_already_exist_success() throws UseException {
        //Given
        UserEntity user = userService.saveUser(new SetUserInfo("berit", "123456"));

        //when
        UseException userException = assertThrows(UseException.class, () -> {
            userService.saveUser(new SetUserInfo("berit", "123456"));
        });

        //Then
        assertThat(userException.getActivity(), is(Activity.CREATE_USER));
        assertThat(userException.getUserExceptionType(), is(UseExceptionType.USER_NAME_ALREADY_EXISTS));
        assertNotNull(user.getToken());
        assertEquals("berit", user.getUsername());
        assertNotNull(user.getPassword());
    }


    @Test
    @Transactional
    void test_verify_login_success() throws UseException {
        //Given
        UserEntity userAnna = userService.saveUser(new SetUserInfo("anna", "losen"));
        UserEntity userBerit = userService.saveUser(new SetUserInfo("berit", "123456"));
        UserEntity userKalle = userService.saveUser(new SetUserInfo("kalle", "password"));

        //when
        String loginAnna = userService.verifyLogin("anna", "losen");
        String loginBerit = userService.verifyLogin("berit", "123456");
        String loginKalle = userService.verifyLogin("kalle", "password");

        //then
        assertEquals(userAnna.getToken(), loginAnna);
        assertEquals(userBerit.getToken(), loginBerit);
        assertEquals(userKalle.getToken(), loginKalle);

    }

    @Test
    void test_get_users() throws UseException {
        //Given
        userService.saveUser(new SetUserInfo("anna", "losen"));
        userService.saveUser(new SetUserInfo("berit", "123456"));
        userService.saveUser(new SetUserInfo("kalle", "password"));

        //When
        List<UserEntity> users = userService.getUsers().collect(Collectors.toList());

        //Then
        assertEquals(3, users.size());
        assertEquals("berit", users.get(1).getUsername());
        assertNotNull(users.get(2).getPassword());
        assertEquals("anna", users.get(0).getUsername());

    }

    @Test
    @Transactional
    void test_Verify_login_and_get_token() throws UseException {
        //Given
        UserEntity userAnna = userService.saveUser(new SetUserInfo("anna", "losen"));
        UserEntity userBerit = userService.saveUser(new SetUserInfo("berit", "123456"));
        UserEntity userKalle = userService.saveUser(new SetUserInfo("kalle", "password"));

        //when
        String tokenResponseToLoginAnna = userService.verifyLogin("anna", "losen");
        System.out.println(ANSI_RED + "Annas token is: " + tokenResponseToLoginAnna + ANSI_RESET);
        String tokenResponseToLoginBerit = userService.verifyLogin("berit", "123456");
        String tokenResponseToLoginKalle = userService.verifyLogin("kalle", "password");

        //Then
        assertEquals(userAnna.getToken(), tokenResponseToLoginAnna);
        assertEquals(userBerit.getToken(), tokenResponseToLoginBerit);
        assertEquals(userKalle.getToken(), tokenResponseToLoginKalle);
    }

    @Test
    @Transactional
    void test_Verify_login_and_get_token_get_exception() throws UseException {
        //Given
        userService.saveUser(new SetUserInfo("anna", "losen"));

        //when
        UseException userException = assertThrows(UseException.class, () -> {
            userService.verifyLogin("anna", "loosen");
        });

        //Then
        assertThat(userException.getActivity(), is(Activity.VERIFY_USER));
        assertThat(userException.getUserExceptionType(), is(UseExceptionType.USER_PASSWORD_INCORRECT));
    }


    @Test
    void verify_token() throws UseException {
        //given
        UserEntity userAnna = userService.saveUser(new SetUserInfo("anna", "losen"));

        //when
        boolean isVerified = userService.verifyToken(userAnna.getToken());

        //Then
        assertTrue(isVerified);
    }

    @Test
    void verify_token_failed() {
        //given
        String token = UUID.randomUUID().toString();

        //when
        boolean isVerified = userService.verifyToken(token);

        //Then
        assertFalse(isVerified);
    }

    @Test
    void test_resources_and_rights_from_token() throws UseException {
        //given
        UserEntity userAnna = userService.saveUser(new SetUserInfo("anna", "losen"));

        //when
        userAnna.addResourceAndRight(new ResourcesRightsEntity(UUID.randomUUID().toString(), Resource.ACCOUNT, Right.READ, userAnna));

        //then
        assertEquals("anna", userAnna.getUsername());
        assertEquals(Resource.ACCOUNT, userAnna.getResourcesRights().get(0).getResource());
        assertEquals(Right.READ, userAnna.getResourcesRights().get(0).getRight());

    }

    @Test
    @Transactional
    void test_get_rights_by_resource_from_token() throws UseException {
        //given
        UserEntity userAnna = userService.saveUser(new SetUserInfo("anna", "losen"));

        //when
        userService.addResourceAndRight("anna", Resource.ACCOUNT, Right.READ);
        List<Right> rights = userService.getRightsByResourceFromToken(userAnna.getToken(), Resource.ACCOUNT);

        //then
        assertEquals(Right.READ, rights.get(0));
    }
}




