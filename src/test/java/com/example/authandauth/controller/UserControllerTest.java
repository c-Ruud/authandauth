package com.example.authandauth.controller;

import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.domain.User;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.repository.UserRepository;
import com.example.authandauth.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("integrationtest")
class UserControllerTest {

    @Autowired
    UserController userController;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @Test
    void test_create_user() throws UseException {
        //Given
        User user = userController.createUser(new SetUserInfo("Kalle", "password"));

        //Then
        assertNotNull(user.getToken());
        assertEquals("Kalle", user.getUsername());
        assertNotNull(user.getPassword());

    }
    @Test
    void test_get_users() throws UseException {
        //Given
        userController.createUser(new SetUserInfo("kalle", "password"));
        userController.createUser(new SetUserInfo("wille", "qwerty"));
        userController.createUser(new SetUserInfo("berit", "password3"));

        //When
        List<User> users = userController.getUsers();

        //Then
        assertEquals(3, users.size());
        assertEquals("wille", users.get(1).getUsername());
        assertNotEquals("krypterat", users.get(2).getPassword());
        assertEquals("kalle", users.get(0).getUsername());

    }

    @Test
    @Transactional
    void verifyUserLogin() throws UseException {
        //given
        userController.createUser(new SetUserInfo("anna", "losen"));

        //when
        String verifiedToken = userController.verifyUserLogin("anna", "losen");

        //Then
        assertNotNull(verifiedToken);
    }
}
