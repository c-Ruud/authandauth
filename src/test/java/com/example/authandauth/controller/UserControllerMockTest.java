package com.example.authandauth.controller;

import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.domain.User;
import com.example.authandauth.entity.UserEntity;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.repository.UserRepository;
import com.example.authandauth.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("integrationtest")
class UserControllerMockTest {

    @Autowired
    UserController userController;
    @MockBean
    UserRepository userRepository;
    @MockBean
    UserServiceImpl userService;

    @Test
    void test_create_user_success() throws UseException {
        //given
        when(userService.saveUser(any(SetUserInfo.class))).thenReturn(new UserEntity("1","anna", "losen", "43"));

        //when
        User user = userController.createUser(new SetUserInfo("anna", "losen"));

        //then
        assertNotNull(user.getToken());
        assertEquals("anna", user.getUsername());

    }

    @Test
    void getUsers() {
        //given
        UserEntity userAnna = new UserEntity("1", "anna", "losen", "32");
        UserEntity userBerit = new UserEntity("2", "berit", "123456", "32");
        UserEntity userKalle = new UserEntity("3", "kalle", "password", "32");
        when(userService.getUsers()).thenReturn(Stream.of(userAnna, userBerit, userKalle));

        //When
        List<User> usersList = userController.getUsers();

        //Then
        assertEquals(3, usersList.size());
        assertEquals("kalle", usersList.get(2).getUsername());
        assertEquals("1", usersList.get(0).getToken());
    }

    @Test
    void verifyUserLogin() throws UseException {
        //given
        when(userService.verifyLogin(eq("anna"), eq("losen"))).thenReturn(("2315"));

        //when
        String verifiedToken = userController.verifyUserLogin("anna", "losen");

        //Then
        assertEquals("2315", verifiedToken);
    }
}
