package com.example.authandauth.controller;

import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.domain.User;
import com.example.authandauth.entityToDto.UserEntityToDto;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController implements UserEntityToDto {

   UserServiceImpl userService;

    @PostMapping("/createUser")
    public User createUser(@RequestBody SetUserInfo userInfo) throws UseException {
        return UserEntityToDto(userService.saveUser(userInfo));
    }

    @GetMapping("/users")
    public List<User> getUsers() {
        return userService.getUsers().map(this::UserEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("verifyUser")
    public String verifyUserLogin(String username, String password) throws UseException {
        return userService.verifyLogin(username, password);
    }
}
