package com.example.authandauth.service;

import com.example.authandauth.domain.Resource;
import com.example.authandauth.domain.Right;
import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.entity.ResourcesRightsEntity;
import com.example.authandauth.entity.UserEntity;
import com.example.authandauth.exceptions.Activity;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.exceptions.UseExceptionType;
import com.example.authandauth.repository.ResourcesRightsRepository;
import com.example.authandauth.repository.UserRepository;
import com.example.authandauth.securePassword.PasswordUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static com.example.authandauth.securePassword.PasswordUtils.verifyPassword;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ResourcesRightsRepository resourcesRightsRepository;

    public UserServiceImpl(UserRepository userRepository,
                           ResourcesRightsRepository resourcesRightsRepository) {
        this.userRepository = userRepository;
        this.resourcesRightsRepository = resourcesRightsRepository;
    }

    @Override
    public UserEntity saveUser(SetUserInfo user) throws UseException {
        String token = UUID.randomUUID().toString();
        String salt = PasswordUtils.generateSalt(512).get();
        // System.out.printf("salt" + salt);

        if (checkIfUsernameAlreadyExist(user.getUsername())) {
            throw new UseException(Activity.CREATE_USER, UseExceptionType.USER_NAME_ALREADY_EXISTS);
        } else {
            UserEntity userEntity = new UserEntity(
                    token,
                    user.getUsername(),
                    hashPassword(user.getPassword(), salt),
                    salt
            );

            log.info("Saving new user {} to the database", user.getUsername());
            userRepository.save(userEntity);
            return userEntity;
        }
    }
    private boolean checkIfUsernameAlreadyExist(String username) {
        return userRepository.findAll().stream().anyMatch(user -> user.getUsername().equals(username));
    }

    @Override
    public String verifyLogin(String username, String password) throws UseException {
        String token = getToken(username);
        UserEntity user = userRepository.getById(token);
        String usersSalt = user.getSalt();

        boolean isVerified = verifyPassword(password, user.getPassword(), usersSalt);
        if (!isVerified) {
            throw new UseException(Activity.VERIFY_USER, UseExceptionType.USER_PASSWORD_INCORRECT);
        }
        return token;
    }

    @Override
    public boolean verifyToken(String token) {
        return userRepository.findAll().stream()
                .anyMatch(userEntity -> userEntity.getToken().equals(token));
    }


    @Override
    public String getToken(String username) {
        return userRepository.findAll().stream()
                .filter(user -> user.getUsername().equals(username)).map(UserEntity::getToken).findFirst().get();
    }

    public String getSalt(String token) {
        return userRepository.findAll().stream()
                .filter(user -> user.getToken().equals(token)).map(UserEntity::getSalt).findFirst().get();
    }

    @Override
    public Stream<UserEntity> getUsers() {
        return userRepository.findAll().stream();
    }

    @Override
    public void addResourceAndRight(String username, Resource resource, Right right) throws UseException {
        String token = getToken(username);
        UserEntity user = getUser(token);

        ResourcesRightsEntity resourcesRightsEntity = new ResourcesRightsEntity(
                UUID.randomUUID().toString(), resource, right, user);

        resourcesRightsRepository.save(resourcesRightsEntity);
        user.addResourceAndRight(resourcesRightsEntity);
        userRepository.save(user);
    }

    @Override
    public List<Right> getRightsByResourceFromToken(String token, Resource resource) throws UseException {
       return userRepository.getById(token).getRightFromResource(resource);
    }

    private UserEntity getUser(String token) throws UseException {
        return userRepository.findById(token).orElseThrow(() -> new UseException(Activity.VERIFY_USER, UseExceptionType.USER_NOT_FOUND));
    }

    String hashPassword(String password, String salt) {
        return PasswordUtils.hashPassword(password, salt).get();
    }
}
