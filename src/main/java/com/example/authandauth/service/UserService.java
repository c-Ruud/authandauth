package com.example.authandauth.service;

import com.example.authandauth.domain.Resource;
import com.example.authandauth.domain.Right;
import com.example.authandauth.domain.SetUserInfo;
import com.example.authandauth.entity.UserEntity;
import com.example.authandauth.exceptions.UseException;

import java.util.List;
import java.util.stream.Stream;

public interface UserService {

    UserEntity saveUser(SetUserInfo user) throws UseException;

    String verifyLogin(String username, String password) throws UseException;

    boolean verifyToken(String token);

    String getToken(String username);

    Stream<UserEntity> getUsers();

    void addResourceAndRight(String username, Resource resource, Right right) throws UseException;

    List<Right> getRightsByResourceFromToken(String token, Resource resource ) throws UseException ;
}
