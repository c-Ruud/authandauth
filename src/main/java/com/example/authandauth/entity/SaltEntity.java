package com.example.authandauth.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "salt")
//@Table
@NoArgsConstructor
@Data
public class SaltEntity {
    @Id
    private String token;
    @Column(length = 1000)
    private String salt;

    public SaltEntity(String token, String salt) {
        this.token = token;
        this.salt = salt;
    }
}
