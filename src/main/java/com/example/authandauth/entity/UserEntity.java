package com.example.authandauth.entity;

import com.example.authandauth.domain.Resource;
import com.example.authandauth.domain.Right;
import com.example.authandauth.exceptions.Activity;
import com.example.authandauth.exceptions.UseException;
import com.example.authandauth.exceptions.UseExceptionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "user")
@Table
@NoArgsConstructor
@Getter
@Setter
public class UserEntity {
    @Id
    private String token;
    private String username;
    private String password;

    @Column(length = 1000)
    private String salt;

    @OneToMany(mappedBy = "userEntity")
    List<ResourcesRightsEntity> resourcesRights;

    public UserEntity(String token,
                      String username,
                      String password,
                      String salt) {
        this.token = token;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.resourcesRights = new ArrayList<>();
    }

    public void addResourceAndRight(ResourcesRightsEntity resourceRight) {
        resourcesRights.add(resourceRight);
    }

    public List<Right> getRightFromResource(Resource resource) throws UseException {
        List<Right> resourceList = resourcesRights.stream()
                .filter(resourcesRights -> resourcesRights.getResource().equals(resource)).map(ResourcesRightsEntity::getRight).collect(Collectors.toList());

        if (!resourceList.isEmpty()) {
            return resourceList;
        } else {
            throw new UseException(Activity.VERIFY_USER_RIGHTS, UseExceptionType.RESOURCE_NOT_FOUND);
        }
    }


}
