package com.example.authandauth.entity;

import com.example.authandauth.domain.Resource;
import com.example.authandauth.domain.Right;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "bajskorv")
//@Table(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class ResourcesRightsEntity {
    @Id
    String idt;

    @Enumerated(EnumType.STRING)
    @Column(name = "resourcename")
    Resource resource;

    @Enumerated(EnumType.STRING)
    @Column(name = "rightname")
    Right right;

    @ManyToOne
    @JoinColumn(name = "user_id")
    UserEntity userEntity;

}
