package com.example.authandauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthandauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthandauthApplication.class, args);
    }

}
