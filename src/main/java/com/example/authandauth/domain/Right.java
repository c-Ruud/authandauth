package com.example.authandauth.domain;

public enum Right {
    READ,
    WRITE,
    EXECUTE
    ;
}
