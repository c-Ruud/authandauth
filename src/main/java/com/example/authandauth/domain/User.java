package com.example.authandauth.domain;

import com.example.authandauth.entity.ResourcesRightsEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class User {
    String token;
    String username;
    String password;
    List<ResourcesRightsEntity> resources;

    @JsonCreator
    public User(
            @JsonProperty("token") String token,
            @JsonProperty("username") String username,
            @JsonProperty("password") String password,
            @JsonProperty("resources") List<ResourcesRightsEntity> resources)
    {
        this.token = token;
        this.username = username;
        this.password = password;
        this.resources = resources;
    }
}
