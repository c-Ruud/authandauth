package com.example.authandauth.domain;

import lombok.Value;

@Value
public class Salt {
    String token;
    String salt;
}
