package com.example.authandauth.repository;

import com.example.authandauth.entity.SaltEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaltRepository extends JpaRepository<SaltEntity, String> {
}
