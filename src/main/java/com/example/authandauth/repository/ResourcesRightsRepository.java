package com.example.authandauth.repository;

import com.example.authandauth.entity.ResourcesRightsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourcesRightsRepository extends JpaRepository<ResourcesRightsEntity, String> {
}
