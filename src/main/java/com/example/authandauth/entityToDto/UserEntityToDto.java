package com.example.authandauth.entityToDto;

import com.example.authandauth.domain.User;
import com.example.authandauth.entity.UserEntity;
import org.springframework.stereotype.Service;

@Service
public interface UserEntityToDto {
    default User UserEntityToDto(UserEntity userEntity) {
        return new User(
                userEntity.getToken(),
                userEntity.getUsername(),
                userEntity.getPassword(),
                userEntity.getResourcesRights());
    }
}
