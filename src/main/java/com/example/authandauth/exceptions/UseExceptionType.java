package com.example.authandauth.exceptions;

public enum UseExceptionType {
    USER_NOT_FOUND,
    USER_NAME_ALREADY_EXISTS,
    USER_PASSWORD_INCORRECT,
    RESOURCE_NOT_FOUND,
    ;

}
