package com.example.authandauth.exceptions;

public enum Activity {
    CREATE_USER,
    VERIFY_USER,
    VERIFY_USER_RIGHTS
}
